-- get all statuses, not repeating, alphabetically ordered
-- SELECT status FROM tasks GROUP BY status ORDER BY status;

-- get the count of all tasks in each project, order by tasks count descending
-- SELECT id pid,name,(select count(*) FROM tasks WHERE project_id=pid) c FROM projects ORDER BY c desc

-- get the count of all tasks in each project, order by projects names
-- SELECT id pid,name,(select count(*) FROM tasks WHERE project_id=pid) c FROM projects ORDER BY name

-- get the tasks for all projects having the name beginning with "N" letter
-- SELECT * FROM tasks WHERE name like "H%"

-- get the list of all projects containing the 'a' letter in the middle of the name, and show the tasks count near each project. Mention that there can exist projects without tasks and tasks with project_id = NULL
-- SELECT id pid, name,(select count(*) FROM tasks WHERE project_id=pid) c FROM tasks WHERE name like "%a%"

-- get the list of tasks with duplicate names. Order alphabetically
-- SELECT id,name FROM tasks GROUP BY name HAVING count(*)>1

-- get list of tasks having several exact matches of both name and status, from the project 'Garage'. Order by matches count
-- не понял что необходимо сделать

-- get the list of project names having more than 10 tasks in status 'completed'. Order by project_id
-- SELECT *,count(*) c FROM projects LEFT JOIN tasks ON projects.id=tasks.project_id WHERE tasks.status=1 GROUP BY projects.id HAVING c>10 ORDER BY project_id