package com.example.ToDoDemo.repo

import com.example.ToDoDemo.entity.Project
import com.example.ToDoDemo.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProjectRepository: JpaRepository<Project, Long> {
    fun findByUser(user: User): MutableList<User>
}