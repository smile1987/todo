package com.example.ToDoDemo.repo

import com.example.ToDoDemo.entity.Project
import com.example.ToDoDemo.entity.Task
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TaskRepository: JpaRepository<Task, Long> {
    fun findByProject(project: Project): MutableList<Task>

}