package com.example.ToDoDemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@SpringBootApplication
class ToDoDemoApplication

fun main(args: Array<String>) {
	runApplication<ToDoDemoApplication>(*args)
}
