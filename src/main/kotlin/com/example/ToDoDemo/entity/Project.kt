package com.example.ToDoDemo.entity

import com.fasterxml.jackson.annotation.*
import javax.persistence.*

@Entity
@Table(name = "projects")
data class Project(
        @OneToMany(mappedBy = "project", cascade = [CascadeType.PERSIST, CascadeType.MERGE])
        @JsonManagedReference
        val tasks: MutableList<Task> = mutableListOf(),

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id")
        @JsonBackReference
        var user: User?
) : BaseEntity() {
    override fun toString(): String {
        return "Project(tasks=$tasks)"
    }
}