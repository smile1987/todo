package com.example.ToDoDemo.entity

import javax.persistence.*

@MappedSuperclass
open class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long = 0

    @Column(name = "name")
    var name: String = ""

    @Column(name = "status")
    var status: Status = Status.DISABLE

}
