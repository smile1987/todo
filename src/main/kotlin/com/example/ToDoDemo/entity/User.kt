package com.example.ToDoDemo.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.*
import javax.print.DocFlavor.STRING
import javax.persistence.JoinColumn
import javax.persistence.CollectionTable
import javax.persistence.FetchType
import javax.persistence.ElementCollection



@Entity
@Table(name = "users")
class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long? = null

    lateinit var username: String

    @JsonIgnore
    lateinit var password: String

    var active: Boolean = true

    @ElementCollection(targetClass = Role::class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = [JoinColumn(name = "user_id")])
    @Enumerated(EnumType.STRING)
    private val roles: MutableList<Role> = mutableListOf(Role.USER)

    @OneToMany(mappedBy = "user", cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JsonManagedReference
    val projects: MutableList<Project> = mutableListOf()
}