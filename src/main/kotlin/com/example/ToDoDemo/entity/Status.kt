package com.example.ToDoDemo.entity

enum class Status {
    ENABLE, DISABLE, DELETE
}