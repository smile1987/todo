package com.example.ToDoDemo.entity

enum class Role {
    ADMIN, USER
}