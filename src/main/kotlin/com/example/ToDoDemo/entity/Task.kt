package com.example.ToDoDemo.entity

import com.fasterxml.jackson.annotation.*
import javax.persistence.*

@Entity
@Table(name = "tasks")
data class Task(
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "project_id")
        @JsonBackReference
        val project: Project? = null,

        @GeneratedValue(strategy = GenerationType.AUTO)
        var sort: Long? = null
): BaseEntity()