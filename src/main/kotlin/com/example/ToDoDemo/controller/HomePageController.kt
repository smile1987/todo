package com.example.ToDoDemo.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/")
class HomePageController {

    @GetMapping
    fun getHomePage(): String{
        return "index"
    }

}