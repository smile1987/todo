package com.example.ToDoDemo.controller

import com.example.ToDoDemo.entity.User
import com.example.ToDoDemo.repo.UserRepository
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/registration")
class RegistrationController(val userRepository: UserRepository) {

    @GetMapping
    fun showRegistration(model: Model): String{
        model.addAttribute("user", User())
        return "registration"
    }

    @PostMapping
    fun addUser(user: User, model: Model): String{

        val userDb = userRepository.findByUsername(user.username)

        userDb?.let{
            model.addAttribute("message", "User exist!")
            return "registration"
        }

        userRepository.save(user)

        return "redirect:/login"
    }
}