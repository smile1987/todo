package com.example.ToDoDemo.controller

import com.example.ToDoDemo.entity.Project
import com.example.ToDoDemo.entity.Task
import com.example.ToDoDemo.repo.TaskRepository
import com.example.ToDoDemo.repo.UserRepository
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping("/api/task")
class TaskController(val taskRepository: TaskRepository, val userRepository: UserRepository
) {

    @PostMapping
    fun createTask(auth: Principal, @RequestBody task: Task): Any? {
        println("createTask")
        println(task.toString())
        val user = userRepository.findByUsername(auth.name)
        task.project!!.user = user!!
//        return null
        return taskRepository.save(task)
    }

    @DeleteMapping("{id}")
    fun deleteTask(@PathVariable("id") task: Task) {
        taskRepository.delete(task)
    }

    @PutMapping("{id}")
    fun editTask(@PathVariable("id") oldTask: Long, @RequestParam("field") field: String, @RequestBody newTask: Task): Any?{
        println("editTask()")

        val task = taskRepository.getOne(oldTask)

        when (field) {
            "status" -> {

                println(task.status.toString() +  "="+ newTask.status)
                task.status = newTask.status

            }
            "name" ->{
                println(task.name + "="+ newTask.name)
                task.name = newTask.name
            }
            else -> {
            }
        }

        return taskRepository.save(task)
    }
}