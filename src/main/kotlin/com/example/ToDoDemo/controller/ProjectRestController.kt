package com.example.ToDoDemo.controller

import com.example.ToDoDemo.entity.Project
import com.example.ToDoDemo.repo.ProjectRepository
import com.example.ToDoDemo.repo.UserRepository
import org.springframework.web.bind.annotation.*
import java.security.Principal
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity



@RestController
@RequestMapping("api/project")
class ProjectRestController(val projectRepository: ProjectRepository, val userRepository: UserRepository) {

    @GetMapping
    fun getProject(auth: Principal): Any {
        println(auth.name + " :: getProjects")
        val user = userRepository.findByUsername(auth.name)
        user?.let {
            val projects = projectRepository.findByUser(it)
            return projects
        }

        return ResponseEntity<Any>(HttpStatus.BAD_REQUEST)
    }

    @PostMapping
    fun createProject(auth: Principal): Any {
        println("create project")
        val user = userRepository.findByUsername(auth.name)
        val project = Project(user = user!!)
        project.name = "New task"
        return projectRepository.save(project)
    }

    @PutMapping("{id}")
    fun updateProject(@PathVariable id: Long, @RequestBody projectName: String): Any? {
        println("updateProject")
        val project = projectRepository.getOne(id)
        project.name = projectName
        projectRepository.save(project)
        return projectName
    }

    @DeleteMapping("{id}")
    fun deleteProject(@PathVariable("id") project: Project){
        projectRepository.delete(project)
    }
}