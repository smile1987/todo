package com.example.ToDoDemo.controller

import com.example.ToDoDemo.repo.UserRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/api/user")
class UserController(val userRepository: UserRepository) {

    @GetMapping
    fun getUser(auth: Principal): Any?{
        val user = userRepository.findByUsername(auth.name)
        println(user)
        return user
    }
}