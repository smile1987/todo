var projectApi = Vue.resource('/api/project{/id}');
var taskApi = Vue.resource('/api/task{/id}');
var userApi = Vue.resource('/api/user{/id}');

Vue.http.headers.common['X-CSRF-TOKEN'] = csrftoken

function getIndex(list, id) {
    for (var i = 0; i < list.length; i++ ) {
        if (list[i].id === id) {
            return i;
        }
    }

    return -1;
}

var task = Vue.component("task",{
    props: ['task'],
    template:
            '<div class="task-item">' +
                '<div class="item-content">'+
                    '<div class="check"><input type="checkbox" v-model="task.status" true-value="ENABLE"  false-value="DISABLE" @change="changeStatus()"></div>' +
                    '<div class="col caption"><input disabled v-model="task.name" class="input-name" ref="taskName" @keyup.enter="editCaption()"/></div>' +
                    '<div class="edit">' +
                        '<i class="fas fa-pen"  @click="editTask()"></i>' +
                        '<i class="fas fa-trash-alt"  @click="deleteTask()"></i>' +
                    '</div>' +
                '</div>' +
            '</div>',
    methods: {
        editTask: function(){
            this.$refs["taskName"].disabled = false
            this.$refs["taskName"].focus()
            this.$refs["taskName"].select()
        },
        
        editCaption: function () {
            var task = {"name": this.task.name}
            console.log(this.task)
            taskApi.update({id: this.task.id,"field":"name"}, task).then(result => {
                if(result.ok){
                    this.$refs["taskName"].disabled = true
                    result.json().then(data => { task = data })
                }
            })
        },

        changeStatus: function () {

            var task = {"status": this.task.status }

            console.log(task)

            taskApi.update({id:this.task.id,"field":"status"},task).then(result =>{
                if(result.ok){
                    result.json().then(data => { task = data })
                }
            })
        },

        deleteTask: function () {

            taskApi.delete({id: this.task.id}).then(result => {
                if (result.ok) {
                    this.$emit("delete", this.task.id);
                }
            })
        },

    }
})

var tasks = Vue.component("tasks", {
    props: ['tasks'],

    template:
            '<div>' +
                '<template v-for="task in tasks">' +
                    '<task :task="task" @delete="onDelete"></task>' +
            '</template>' +
            '</div>',
    methods:{
        onDelete: function (task_id) {
            var index = getIndex(this.tasks, task_id)
            this.tasks.splice(index, 1)
        }
    },
})


var projects = Vue.component('projects',{
    props: ['projects'],
    template:
            '<div class="projects">' +
                '<div v-for="item in projects">' +
                    '<project :project="item" @delete="deleteProject(item)"/>' +
                '</div>' +
                '<div class="footer"><span id="btn-project-create" @click="createProject"><i class="fas fa-plus"></i>Add TODO List</span></div>' +
            '</div>',
    methods:{
        createProject: function () {
            console.log("create project");
            var project = {"name": "New task",}
            projectApi.save({}, project).then(result =>
            {
                if(result.ok){
                    result.json().then(data =>{
                        this.projects.push(data)
                    })
                }
            })
        },

        deleteProject: function(project){
            console.log("deleteProject()")
            projectApi.delete({id: project.id}).then(result =>
            {if(result.ok){
                var index = getIndex(this.projects, project.id)
                this.projects.splice(index,1)
            }})
        },
    }
})

var project = Vue.component('project',{
    props: ['project'],
    data: function(){
        return{
            projectName: this.project.name,

        }
    },
    template:
        '<div class="project">' +
        '<header class="">' +
            '<div class="icon"><i class="far fa-calendar-alt"></i></div>' +
            '<div class="col caption">' +
                '<input disabled class="input-name" v-model="projectName" ref="projectName" v-on:keyup.enter="saveProjectName">' +
            '</div>' +
            '<div class="edit">' +
                '<i class="fas fa-pen" id="btn-project-edit" @click="editProject"></i>' +
                '<i class="fas fa-trash-alt" id="btn-project-delete"  @click="onDelete"></i>' +
            '</div>' +
        '</header>' +
        '<taskbar :tasks="project.tasks" :project_id="project.id"/>' +
        '</div>',
    methods:{

        onDelete: function(){
            this.$emit('delete', this.project)
        },

        editProject: function () {
            this.$refs["projectName"].disabled = false
            this.$refs["projectName"].focus()
            this.$refs["projectName"].select()
        },

        saveProjectName: function () {
            console.log("saveProjectName()")
            this.$refs["projectName"].disabled  = true

            projectApi.update({id: this.project.id}, this.projectName).then(result =>
                console.log(result))
        },
    }
})


var taskBar = Vue.component("taskbar",{
    props: ['tasks','project_id'],
    data: function () {
        return {
            taskName: '',
        }
    },
    template:
            '<div>' +
                '<template>' +
                    '<div class="taskbar">' +
                        '<div class="icon"><i class="fas fa-plus"></i></div>' +
                            '<div class="col">' +
                                '<div class="input-group">\n' +
                                    '<input id="input-task-name" class="form-control" v-model="taskName" v-on:keyup.enter="createTask" placeholder="Start typing " aria-describedby="basic-addon2"/>' +
                                '  <div class="input-group-append">\n' +
                                '    <span class="input-group-text button bg-success text-white" id="basic-addon2" @click="createTask">Add Task</span>\n' +
                                '  </div>\n' +
                                '</div>' +
                            '</div>' +
                    '</div>' +
                    '<div class="tasklist">' +
                    '<tasks :tasks="tasks" :project_id="project_id"/>' +
                    '</div>' +
                '</template>' +
            '</div>',
    methods:{
        createTask: function () {


            if(this.taskName === ''){ return;}

            console.log("create task")

            var project = {id: this.project_id}
            var task = {"name": this.taskName, "project": project}
            taskApi.save({}, task).then(result =>
            {
                if(result.ok){
                    result.json().then(data =>{
                        this.tasks.push(data)
                        this.taskName = ''
                    })
                }else{

                }
            })
        }
    }
})


var ToDoApp = new Vue({
    el: '#ToDoApp',

    template: "<projects :projects='this.projects'/>",

    data: function () {
        return {
            projects: [],
            taskName: '',
            tasks: []
        }
    },

    created: function () {

        userApi.get().then(result =>
            result.json().then(data =>{
                console.log(data)
                this.projects = data.projects
        })
        )

        // projectApi.get().then(result =>
        // {
        //     console.log(result)
        //     result.json().then(data => {
        //         this.project_id = data.id;
        //         this.projectName = data.name;
        //         this.tasks = data.tasks
        //     })}
        // )
    },
});